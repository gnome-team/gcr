# German gcr translation.
# Copyright (C) 2003, 2004 Free Software Foundation, Inc.
# This file is distributed under the same license as the gcr package.
#
#
# Christian Neumair <chris@gnome-de.org>, 2003, 2004.
# Hendrik Richter <hendrikr@gnome.org>, 2006.
# Philipp Kerling <k.philipp@gmail.com>, 2008.
# Stefan Horning <stefan@hornings.de>, 2009.
# Nathan-J. Hirschauer <nathanhirschauer@verfriemelt.org>, 2010.
# Christian Kirbach <Christian.Kirbach@googlemail.com>, 2008-2010, 2012.
# Tobias Endrigkeit <tobiasendrigkeit@googlemail.com>, 2012.
# Wolfgang Stöggl <c72578@yahoo.de>, 2010, 2011, 2014.
# Benjamin Steinwender <b@stbe.at>, 2014.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2009-2013, 2016, 2018, 2020.
# Tim Sabsch <tim@sabsch.com>, 2020.
# Philipp Kiemle <philipp.kiemle@gmail.com>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-keyring master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gcr/issues\n"
"POT-Creation-Date: 2021-04-26 17:11+0000\n"
"PO-Revision-Date: 2021-07-26 15:23+0200\n"
"Last-Translator: Philipp Kiemle <philipp.kiemle@gmail.com>\n"
"Language-Team: Deutsch <gnome-de@gnome.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.4.2\n"

#: egg/egg-oid.c:40
msgid "Domain Component"
msgstr "Domänenkomponente"

#: egg/egg-oid.c:42 ui/gcr-gnupg-renderer.c:410 ui/gcr-gnupg-renderer.c:579
msgid "User ID"
msgstr "Benutzerkennung"

#: egg/egg-oid.c:45
msgid "Email Address"
msgstr "E-Mail-Adresse"

#: egg/egg-oid.c:53
msgid "Date of Birth"
msgstr "Geburtsdatum"

#: egg/egg-oid.c:55
msgid "Place of Birth"
msgstr "Geburtsort"

#: egg/egg-oid.c:57
msgid "Gender"
msgstr "Geschlecht"

#: egg/egg-oid.c:59
msgid "Country of Citizenship"
msgstr "Staatsangehörigkeit"

#: egg/egg-oid.c:61
msgid "Country of Residence"
msgstr "Aufenthaltsland"

#: egg/egg-oid.c:64
msgid "Common Name"
msgstr "Vorname"

#: egg/egg-oid.c:66
msgid "Surname"
msgstr "Nachname"

#: egg/egg-oid.c:68 ui/gcr-certificate-renderer.c:565
msgid "Serial Number"
msgstr "Seriennummer"

#: egg/egg-oid.c:70
msgid "Country"
msgstr "Land"

#: egg/egg-oid.c:72
msgid "Locality"
msgstr "Ort"

#: egg/egg-oid.c:74
msgid "State"
msgstr "Staat"

#: egg/egg-oid.c:76
msgid "Street"
msgstr "Straße"

#: egg/egg-oid.c:78
msgid "Organization"
msgstr "Organisation"

#: egg/egg-oid.c:80
msgid "Organizational Unit"
msgstr "Organisationseinheit"

#: egg/egg-oid.c:82
msgid "Title"
msgstr "Titel"

#: egg/egg-oid.c:84
msgid "Telephone Number"
msgstr "Telefonnummer"

#: egg/egg-oid.c:86
msgid "Given Name"
msgstr "Vorname"

#: egg/egg-oid.c:88
msgid "Initials"
msgstr "Initialen"

#: egg/egg-oid.c:90
msgid "Generation Qualifier"
msgstr "Erstellungsvermerk"

#: egg/egg-oid.c:92
msgid "DN Qualifier"
msgstr "DN-Kennzeichnung"

#: egg/egg-oid.c:94
msgid "Pseudonym"
msgstr "Pseudonym"

#. Translators: Russian: Main state registration number
#: egg/egg-oid.c:98
msgid "OGRN"
msgstr "OGRN"

#. Translators: Russian: Individual insurance account number
#: egg/egg-oid.c:101
msgid "SNILS"
msgstr "SNILS"

#. Translators: Russian: Main state registration number for individual enterpreneurs
#: egg/egg-oid.c:104
msgid "OGRNIP"
msgstr "OGRNIP"

#. Translators: Russian: Individual taxpayer number
#: egg/egg-oid.c:107
msgid "INN"
msgstr "INN"

#: egg/egg-oid.c:110 ui/gcr-gnupg-renderer.c:201 ui/gcr-key-renderer.c:385
msgid "RSA"
msgstr "RSA"

#: egg/egg-oid.c:111
msgid "MD2 with RSA"
msgstr "MD2 mit RSA"

#: egg/egg-oid.c:112
msgid "MD5 with RSA"
msgstr "MD5 mit RSA"

#: egg/egg-oid.c:113
msgid "SHA1 with RSA"
msgstr "SHA1 mit RSA"

#: egg/egg-oid.c:115 ui/gcr-gnupg-renderer.c:205 ui/gcr-key-renderer.c:387
msgid "DSA"
msgstr "DSA"

#: egg/egg-oid.c:116
msgid "SHA1 with DSA"
msgstr "SHA1 mit DSA"

#: egg/egg-oid.c:118 ui/gcr-key-renderer.c:389
msgid "Elliptic Curve"
msgstr "Elliptische Kurve"

#: egg/egg-oid.c:119
msgid "SHA1 with ECDSA"
msgstr "SHA1 mit ECDSA"

#: egg/egg-oid.c:120
msgid "SHA224 with ECDSA"
msgstr "SHA224 mit ECDSA"

#: egg/egg-oid.c:121
msgid "SHA256 with ECDSA"
msgstr "SHA256 mit ECDSA"

#: egg/egg-oid.c:122
msgid "SHA384 with ECDSA"
msgstr "SHA384 mit ECDSA"

#: egg/egg-oid.c:123
msgid "SHA512 with ECDSA"
msgstr "SHA512 mit ECDSA"

#: egg/egg-oid.c:125
msgid "GOST R 34.11-94 with GOST R 34.10-2001"
msgstr "GOST R 34.11-94 mit GOST R 34.10-2001"

#: egg/egg-oid.c:126
msgid "GOST R 34.10-2001"
msgstr "GOST R 34.10-2001"

#: egg/egg-oid.c:127
msgid "GOST R 34.10-2012 256-bit curve"
msgstr "GOST R 34.10-2012 256-bit Kurve"

#: egg/egg-oid.c:128
msgid "GOST R 34.10-2012 512-bit curve"
msgstr "GOST R 34.10-2012 512-bit Kurve"

#: egg/egg-oid.c:129
msgid "GOST R 34.11-2012/256 with GOST R 34.10-2012 256-bit curve"
msgstr "GOST R 34.11-2012/256 mit GOST R 34.10-2012 256-bit Kurve"

#: egg/egg-oid.c:130
msgid "GOST R 34.11-2012/512 with GOST R 34.10-2012 512-bit curve"
msgstr "GOST R 34.11-2012/512 mit GOST R 34.10-2012 512-bit Kurve"

#. Extended Key Usages
#: egg/egg-oid.c:133
msgid "Server Authentication"
msgstr "Server-Legitimierung"

#: egg/egg-oid.c:134
msgid "Client Authentication"
msgstr "Client-Legitimierung"

#: egg/egg-oid.c:135
msgid "Code Signing"
msgstr "Code-Signatur"

#: egg/egg-oid.c:136
msgid "Email Protection"
msgstr "E-Mail-Schutz"

#: egg/egg-oid.c:137
msgid "Time Stamping"
msgstr "Zeitstempel"

#: gck/gck-module.c:332
#, c-format
msgid "Error loading PKCS#11 module: %s"
msgstr "Fehler beim Laden des PKCS#11-Moduls: %s"

#: gck/gck-module.c:346
#, c-format
msgid "Couldn’t initialize PKCS#11 module: %s"
msgstr "PKCS#11-Modul konnte nicht initialisiert werden: %s"

#: gck/gck-modules.c:62
#, c-format
msgid "Couldn’t initialize registered PKCS#11 modules: %s"
msgstr "Registrierte PKCS#11-Module konnten nicht initialisiert werden: %s"

#: gck/gck-uri.c:224
#, c-format
msgid "The URI has invalid encoding."
msgstr "Die Adresse hat eine ungültige Zeichenkodierung."

#: gck/gck-uri.c:228
msgid "The URI does not have the “pkcs11” scheme."
msgstr "Die Adresse entspricht nicht dem Schema »PKCS11«."

#: gck/gck-uri.c:232
msgid "The URI has bad syntax."
msgstr "Die Adresse hat eine ungültige Syntax."

#: gck/gck-uri.c:236
msgid "The URI has a bad version number."
msgstr "Die Adresse hat eine falsche Versionsnummer."

#: gcr/gcr-callback-output-stream.c:56 gcr/gcr-callback-output-stream.c:73
#, c-format
msgid "The stream was closed"
msgstr "Der Datenstrom wurde geschlossen"

#. later
#. later
#: gcr/gcr-certificate.c:350 gcr/gcr-gnupg-key.c:429
msgctxt "column"
msgid "Name"
msgstr "Name"

#: gcr/gcr-certificate.c:352
msgctxt "column"
msgid "Issued By"
msgstr "Ausgestellt von"

#. later
#: gcr/gcr-certificate.c:354
msgctxt "column"
msgid "Expires"
msgstr "Läuft ab"

#: gcr/gcr-certificate.c:1186 gcr/gcr-parser.c:346
#: ui/gcr-certificate-renderer.c:103 ui/gcr-certificate-exporter.c:464
msgid "Certificate"
msgstr "Zertifikat"

#: gcr/gcr-certificate-extensions.c:190
msgid "Other Name"
msgstr "Sonstiger Name"

#: gcr/gcr-certificate-extensions.c:200
msgid "XMPP Addr"
msgstr "XMPP-Adresse"

#: gcr/gcr-certificate-extensions.c:204
msgid "DNS SRV"
msgstr "DNS-Server"

#: gcr/gcr-certificate-extensions.c:216 ui/gcr-gnupg-renderer.c:423
#: ui/gcr-gnupg-renderer.c:705
msgid "Email"
msgstr "E-Mail"

#: gcr/gcr-certificate-extensions.c:224
msgid "DNS"
msgstr "DNS"

#: gcr/gcr-certificate-extensions.c:232
msgid "X400 Address"
msgstr "X400-Adresse"

#: gcr/gcr-certificate-extensions.c:239
msgid "Directory Name"
msgstr "Ordnername"

#: gcr/gcr-certificate-extensions.c:247
msgid "EDI Party Name"
msgstr "Name der EDI-Stelle"

#: gcr/gcr-certificate-extensions.c:254
msgid "URI"
msgstr "Adresse"

#: gcr/gcr-certificate-extensions.c:262
msgid "IP Address"
msgstr "IP-Adresse"

#: gcr/gcr-certificate-extensions.c:270
msgid "Registered ID"
msgstr "Registrierte Kennung"

#: gcr/gcr-certificate-request.c:406
#, c-format
msgid "Unsupported key type for certificate request"
msgstr "Nicht unterstützter Schlüsseltyp für die Anfrage des Zertifikats"

#: gcr/gcr-certificate-request.c:493 gcr/gcr-certificate-request.c:577
#, c-format
msgid "The key cannot be used to sign the request"
msgstr ""
"Dieser Schlüssel kann nicht genutzt werden, um die Anfrage zu unterzeichnen"

#: gcr/gcr-gnupg-importer.c:95
msgid "GnuPG Keyring"
msgstr "GnuPG-Schlüsselbund"

#: gcr/gcr-gnupg-importer.c:97
#, c-format
msgid "GnuPG Keyring: %s"
msgstr "GnuPG-Schlüsselbund: %s"

#: gcr/gcr-gnupg-key.c:143 gcr/gcr-parser.c:352 ui/gcr-gnupg-renderer.c:88
msgid "PGP Key"
msgstr "PGP-Schlüssel"

#: gcr/gcr-gnupg-key.c:431
msgctxt "column"
msgid "Key ID"
msgstr "Schlüsselkennung"

#: gcr/gcr-gnupg-process.c:867
#, c-format
msgid "Gnupg process exited with code: %d"
msgstr "Gnupg-Prozess endete mit Fehlercode: %d"

#: gcr/gcr-gnupg-process.c:874
#, c-format
msgid "Gnupg process was terminated with signal: %d"
msgstr "Gnupg-Prozess wurde mit Signal beendet: %d"

#: gcr/gcr-gnupg-process.c:928 gcr/gcr-parser.c:2598 gcr/gcr-parser.c:3192
#: gcr/gcr-system-prompt.c:932
msgid "The operation was cancelled"
msgstr "Der Vorgang wurde abgebrochen"

#: gcr/gcr-parser.c:343 ui/gcr-key-renderer.c:361
msgid "Private Key"
msgstr "Privater Schlüssel"

#: gcr/gcr-parser.c:349 ui/gcr-certificate-renderer.c:887
#: ui/gcr-gnupg-renderer.c:738 ui/gcr-key-renderer.c:370
msgid "Public Key"
msgstr "Öffentlicher Schlüssel"

#: gcr/gcr-parser.c:355
msgid "Certificate Request"
msgstr "Zertifikatsanfrage"

#: gcr/gcr-parser.c:2601
msgid "Unrecognized or unsupported data."
msgstr "Unbekannte oder nicht unterstützte Daten."

#: gcr/gcr-parser.c:2604
msgid "Could not parse invalid or corrupted data."
msgstr "Ungültige oder defekte Daten konnten nicht verarbeitet werden."

#: gcr/gcr-parser.c:2607
msgid "The data is locked"
msgstr "Die Daten sind gesperrt"

#: gcr/gcr-prompt.c:229
msgid "Continue"
msgstr "Fortsetzen"

#: gcr/gcr-prompt.c:238
msgid "Cancel"
msgstr "Abbrechen"

#: gcr/gcr-ssh-agent-interaction.c:116
#, c-format
msgid "Unlock password for: %s"
msgstr "Passwort zum Entsperren von: %s"

#: gcr/gcr-ssh-agent-interaction.c:152
msgid "Unlock private key"
msgstr "Privaten Schlüssel entsperren"

#: gcr/gcr-ssh-agent-interaction.c:153
msgid "Enter password to unlock the private key"
msgstr "Geben Sie das Passwort zum Entsperren des privaten Schlüssels ein"

#. TRANSLATORS: The private key is locked
#: gcr/gcr-ssh-agent-interaction.c:156
#, c-format
msgid "An application wants access to the private key “%s”, but it is locked"
msgstr ""
"Eine Anwendung versucht, auf den privaten Schlüssel »%s« zuzugreifen, dieser "
"ist jedoch gesperrt"

#: gcr/gcr-ssh-agent-interaction.c:161
msgid "Automatically unlock this key whenever I’m logged in"
msgstr "Diesen Schlüssel beim Anmelden automatisch entsperren"

#: gcr/gcr-ssh-agent-interaction.c:163 ui/gcr-pkcs11-import-dialog.ui:143
#: ui/gcr-unlock-renderer.c:70 ui/gcr-unlock-renderer.c:124
msgid "Unlock"
msgstr "Entsperren"

#: gcr/gcr-ssh-agent-interaction.c:166
msgid "The unlock password was incorrect"
msgstr "Das Passwort zum Entsperren war nicht korrekt"

#: gcr/gcr-ssh-agent-service.c:259
msgid "Unnamed"
msgstr "Unbenannt"

#: gcr/gcr-ssh-askpass.c:194
msgid "Enter your OpenSSH passphrase"
msgstr "Geben Sie Ihr OpenSSH-Kennwort ein"

#: gcr/gcr-subject-public-key.c:405
msgid "Unrecognized or unavailable attributes for key"
msgstr "Unerkanntes oder nicht verfügbares Attribut für den Schlüssel"

#: gcr/gcr-subject-public-key.c:491 gcr/gcr-subject-public-key.c:574
msgid "Couldn’t build public key"
msgstr "Öffentlicher Schlüssel konnte nicht erzeugt werden"

#: gcr/gcr-system-prompt.c:912
msgid "Another prompt is already in progress"
msgstr "Eine andere Anzeige läuft bereits"

#. Translators: A pinned certificate is an exception which
#. trusts a given certificate explicitly for a purpose and
#. communication with a certain peer.
#: gcr/gcr-trust.c:341
#, c-format
msgid "Couldn’t find a place to store the pinned certificate"
msgstr ""
"Es konnte kein Ort für die Hinterlegung des beschränkt gültigen Zertifikats "
"gefunden werden"

#: ui/gcr-certificate-renderer.c:118
msgid "Basic Constraints"
msgstr "Globale Einschränkungen"

#: ui/gcr-certificate-renderer.c:120
msgid "Certificate Authority"
msgstr "Zertifizierungsstelle"

#: ui/gcr-certificate-renderer.c:121 ui/gcr-certificate-renderer.c:958
msgid "Yes"
msgstr "Ja"

#: ui/gcr-certificate-renderer.c:121 ui/gcr-certificate-renderer.c:958
msgid "No"
msgstr "Nein"

#: ui/gcr-certificate-renderer.c:124
msgid "Max Path Length"
msgstr "Maximale Pfadlänge"

#: ui/gcr-certificate-renderer.c:125
msgid "Unlimited"
msgstr "Unbegrenzt"

#: ui/gcr-certificate-renderer.c:144
msgid "Extended Key Usage"
msgstr "Erweiterte Schlüsselverwendung"

#: ui/gcr-certificate-renderer.c:155
msgid "Allowed Purposes"
msgstr "Zugelassene Zwecke"

#: ui/gcr-certificate-renderer.c:175
msgid "Subject Key Identifier"
msgstr "Personen-Schlüsselidentifikator"

#: ui/gcr-certificate-renderer.c:176
msgid "Key Identifier"
msgstr "Schlüsselidentifikator"

#: ui/gcr-certificate-renderer.c:187
msgid "Digital signature"
msgstr "Digitale Signatur"

#: ui/gcr-certificate-renderer.c:188
msgid "Non repudiation"
msgstr "Keine Zurückweisung"

#: ui/gcr-certificate-renderer.c:189
msgid "Key encipherment"
msgstr "Schlüsselverschlüsselung"

#: ui/gcr-certificate-renderer.c:190
msgid "Data encipherment"
msgstr "Datenverschlüsselung"

#: ui/gcr-certificate-renderer.c:191
msgid "Key agreement"
msgstr "Schlüsselaustausch"

#: ui/gcr-certificate-renderer.c:192
msgid "Certificate signature"
msgstr "Zertifikatsignatur"

#: ui/gcr-certificate-renderer.c:193
msgid "Revocation list signature"
msgstr "Signatur der Zertifikatrücknahmeliste"

#: ui/gcr-certificate-renderer.c:194
msgid "Encipher only"
msgstr "Nur verschlüsseln"

#: ui/gcr-certificate-renderer.c:195
msgid "Decipher only"
msgstr "Nur entschlüsseln"

#: ui/gcr-certificate-renderer.c:220
msgid "Key Usage"
msgstr "Schlüsselverwendung"

#: ui/gcr-certificate-renderer.c:221
msgid "Usages"
msgstr "Verwendungszwecke"

#: ui/gcr-certificate-renderer.c:241
msgid "Subject Alternative Names"
msgstr "Alternative Personennamen"

#: ui/gcr-certificate-renderer.c:268
msgid "Extension"
msgstr "Erweiterung"

#: ui/gcr-certificate-renderer.c:272
msgid "Identifier"
msgstr "Bezeichner"

#: ui/gcr-certificate-renderer.c:273 ui/gcr-certificate-request-renderer.c:268
#: ui/gcr-gnupg-renderer.c:414 ui/gcr-gnupg-renderer.c:431
msgid "Value"
msgstr "Wert"

#: ui/gcr-certificate-renderer.c:291
msgid "Couldn’t export the certificate."
msgstr "Zertifikat konnte nicht exportiert werden."

#: ui/gcr-certificate-renderer.c:527 ui/gcr-certificate-request-renderer.c:309
msgid "Identity"
msgstr "Identität"

#: ui/gcr-certificate-renderer.c:531
msgid "Verified by"
msgstr "Überprüft durch"

#: ui/gcr-certificate-renderer.c:538 ui/gcr-gnupg-renderer.c:719
msgid "Expires"
msgstr "Läuft ab"

#. The subject
#: ui/gcr-certificate-renderer.c:545 ui/gcr-certificate-request-renderer.c:315
msgid "Subject Name"
msgstr "Personenname"

#. The Issuer
#: ui/gcr-certificate-renderer.c:550
msgid "Issuer Name"
msgstr "Name des Herausgebers"

#. The Issued Parameters
#: ui/gcr-certificate-renderer.c:555
msgid "Issued Certificate"
msgstr "Zertifikat des Herausgebers"

#: ui/gcr-certificate-renderer.c:560 ui/gcr-certificate-request-renderer.c:326
msgid "Version"
msgstr "Version"

#: ui/gcr-certificate-renderer.c:574
msgid "Not Valid Before"
msgstr "Nicht gültig vor"

#: ui/gcr-certificate-renderer.c:579
msgid "Not Valid After"
msgstr "Nicht gültig nach"

#. Fingerprints
#: ui/gcr-certificate-renderer.c:584
msgid "Certificate Fingerprints"
msgstr "Zertifikat-Fingerabdrücke"

#. Public Key Info
#: ui/gcr-certificate-renderer.c:590 ui/gcr-certificate-request-renderer.c:329
#: ui/gcr-certificate-request-renderer.c:375
msgid "Public Key Info"
msgstr "Informationen zum öffentlichen Schlüssel"

#. Signature
#: ui/gcr-certificate-renderer.c:605 ui/gcr-certificate-renderer.c:915
#: ui/gcr-certificate-request-renderer.c:345
#: ui/gcr-certificate-request-renderer.c:382 ui/gcr-gnupg-renderer.c:560
msgid "Signature"
msgstr "Signatur"

#: ui/gcr-certificate-renderer.c:622
msgid "Export Certificate…"
msgstr "Zertifikat exportieren …"

#: ui/gcr-certificate-renderer.c:861
msgid "Key Algorithm"
msgstr "Schlüssel-Algorithmus"

#: ui/gcr-certificate-renderer.c:866
msgid "Key Parameters"
msgstr "Schlüsselparameter"

#: ui/gcr-certificate-renderer.c:874 ui/gcr-gnupg-renderer.c:353
msgid "Key Size"
msgstr "Schlüssellänge"

#: ui/gcr-certificate-renderer.c:882
msgid "Key SHA1 Fingerprint"
msgstr "SHA1-Fingerabdruck des Schlüssels"

#: ui/gcr-certificate-renderer.c:904
msgid "Signature Algorithm"
msgstr "Signatur-Algorithmus"

#: ui/gcr-certificate-renderer.c:908
msgid "Signature Parameters"
msgstr "Parameter der Signatur"

#: ui/gcr-certificate-renderer.c:957
msgid "Critical"
msgstr "Kritisch"

#. The certificate request type
#: ui/gcr-certificate-request-renderer.c:95
#: ui/gcr-certificate-request-renderer.c:304
#: ui/gcr-certificate-request-renderer.c:319
#: ui/gcr-certificate-request-renderer.c:362
#: ui/gcr-certificate-request-renderer.c:367
msgid "Certificate request"
msgstr "Zertifikatanfrage"

#: ui/gcr-certificate-request-renderer.c:257
msgid "Attribute"
msgstr "Attribut"

#: ui/gcr-certificate-request-renderer.c:261
#: ui/gcr-certificate-request-renderer.c:320
#: ui/gcr-certificate-request-renderer.c:368 ui/gcr-gnupg-renderer.c:591
#: ui/gcr-gnupg-renderer.c:593
msgid "Type"
msgstr "Typ"

#: ui/gcr-certificate-request-renderer.c:372
msgid "Challenge"
msgstr "Aufgabe"

#: ui/gcr-display-view.c:298
msgid "_Details"
msgstr "_Details"

#: ui/gcr-failure-renderer.c:159
#, c-format
msgid "Could not display “%s”"
msgstr "»%s« kann nicht angezeigt werden"

#: ui/gcr-failure-renderer.c:161
msgid "Could not display file"
msgstr "Datei kann nicht angezeigt werden"

#: ui/gcr-failure-renderer.c:166
msgid "Reason"
msgstr "Grund"

#: ui/gcr-failure-renderer.c:216
#, c-format
msgid "Cannot display a file of this type."
msgstr "Eine Datei dieses Typs kann nicht angezeigt werden."

# Verschlüsselungsverfahren
#: ui/gcr-gnupg-renderer.c:203
msgid "Elgamal"
msgstr "Elgamal"

#: ui/gcr-gnupg-renderer.c:216
msgid "Encrypt"
msgstr "Verschlüsseln"

#: ui/gcr-gnupg-renderer.c:218
msgid "Sign"
msgstr "Signieren"

#: ui/gcr-gnupg-renderer.c:220
msgid "Certify"
msgstr "Zertifizieren"

#: ui/gcr-gnupg-renderer.c:222
msgid "Authenticate"
msgstr "Legitimieren"

#: ui/gcr-gnupg-renderer.c:224
msgctxt "capability"
msgid "Disabled"
msgstr "Deaktiviert"

#: ui/gcr-gnupg-renderer.c:255 ui/gcr-gnupg-renderer.c:414
#: ui/gcr-key-renderer.c:391 ui/gcr-key-renderer.c:395
msgid "Unknown"
msgstr "Unbekannt"

#: ui/gcr-gnupg-renderer.c:257
msgid "Invalid"
msgstr "Ungültig"

#: ui/gcr-gnupg-renderer.c:259
msgctxt "ownertrust"
msgid "Disabled"
msgstr "Deaktiviert"

#: ui/gcr-gnupg-renderer.c:261
msgid "Revoked"
msgstr "Widerrufen"

#: ui/gcr-gnupg-renderer.c:263
msgid "Expired"
msgstr "Abgelaufen"

#: ui/gcr-gnupg-renderer.c:265
msgid "Undefined trust"
msgstr "Unbestimmtes Vertrauen"

#: ui/gcr-gnupg-renderer.c:267
msgid "Distrusted"
msgstr "Nicht vertrauenswürdig"

#: ui/gcr-gnupg-renderer.c:269
msgid "Marginally trusted"
msgstr "Teilweise vertrauenswürdig"

#: ui/gcr-gnupg-renderer.c:271
msgid "Fully trusted"
msgstr "Vollständig vertrauenswürdig"

#: ui/gcr-gnupg-renderer.c:273
msgid "Ultimately trusted"
msgstr "Ultimativ vertrauenswürdig"

#: ui/gcr-gnupg-renderer.c:287
msgid "The information in this key has not yet been verified"
msgstr "Die Informationen in diesem Schlüssel wurden noch nicht verifiziert"

#: ui/gcr-gnupg-renderer.c:290
msgid "This key is invalid"
msgstr "Dieser Schlüssel ist ungültig"

#: ui/gcr-gnupg-renderer.c:293
msgid "This key has been disabled"
msgstr "Dieser Schlüssel wurde deaktiviert"

#: ui/gcr-gnupg-renderer.c:296
msgid "This key has been revoked"
msgstr "Dieser Schlüssel wurde wiederrufen"

#: ui/gcr-gnupg-renderer.c:299
msgid "This key has expired"
msgstr "Dieser Schlüssel ist abgelaufen"

#: ui/gcr-gnupg-renderer.c:304
msgid "This key is distrusted"
msgstr "Dieser Schlüssel ist nicht vertrauenswürdig"

#: ui/gcr-gnupg-renderer.c:307
msgid "This key is marginally trusted"
msgstr "Dieser Schlüssel ist teilweise vertrauenswürdig"

#: ui/gcr-gnupg-renderer.c:310
msgid "This key is fully trusted"
msgstr "Der Schlüssel ist vollständig vertrauenswürdig"

#: ui/gcr-gnupg-renderer.c:313
msgid "This key is ultimately trusted"
msgstr "Dieser Schlüssel ist ultimativ vertrauenswürdig"

#: ui/gcr-gnupg-renderer.c:338 ui/gcr-gnupg-renderer.c:564
msgid "Key ID"
msgstr "Schlüsselkennung"

#: ui/gcr-gnupg-renderer.c:346 ui/gcr-gnupg-renderer.c:572
#: ui/gcr-gnupg-renderer.c:619 ui/gcr-key-renderer.c:392
msgid "Algorithm"
msgstr "Algorithmus"

#: ui/gcr-gnupg-renderer.c:361 ui/gcr-gnupg-renderer.c:438
#: ui/gcr-gnupg-renderer.c:481
msgid "Created"
msgstr "Erzeugt"

#: ui/gcr-gnupg-renderer.c:370 ui/gcr-gnupg-renderer.c:447
#: ui/gcr-gnupg-renderer.c:490
msgid "Expiry"
msgstr "Ablaufdatum"

#: ui/gcr-gnupg-renderer.c:379
msgid "Capabilities"
msgstr "Fähigkeiten"

#: ui/gcr-gnupg-renderer.c:392
msgid "Owner trust"
msgstr "Vertrauenswürdigkeit des Besitzers"

#: ui/gcr-gnupg-renderer.c:420
msgid "Name"
msgstr "Name"

#: ui/gcr-gnupg-renderer.c:426 ui/gcr-gnupg-renderer.c:708
msgid "Comment"
msgstr "Kommentar"

#: ui/gcr-gnupg-renderer.c:466
msgid "User Attribute"
msgstr "Benutzerattribut"

#: ui/gcr-gnupg-renderer.c:473 ui/gcr-key-renderer.c:398
msgid "Size"
msgstr "Größe"

#: ui/gcr-gnupg-renderer.c:508
msgid "Signature of a binary document"
msgstr "Signatur des Binär-Dokumentes"

#: ui/gcr-gnupg-renderer.c:510
msgid "Signature of a canonical text document"
msgstr "Signatur des Canonical-Textdokumentes"

#: ui/gcr-gnupg-renderer.c:512
msgid "Standalone signature"
msgstr "Eigenständige Signatur"

#: ui/gcr-gnupg-renderer.c:514
msgid "Generic certification of key"
msgstr "Allgemeine Zertifizierung des Schlüssels"

#: ui/gcr-gnupg-renderer.c:516
msgid "Persona certification of key"
msgstr "Personenbezogene Zertifizierung des Schlüssels"

#: ui/gcr-gnupg-renderer.c:518
msgid "Casual certification of key"
msgstr "Zufällige Zertifizierung des Schlüssels"

#: ui/gcr-gnupg-renderer.c:520
msgid "Positive certification of key"
msgstr "Positive Zertifizierung des Schlüssels"

#: ui/gcr-gnupg-renderer.c:522
msgid "Subkey binding signature"
msgstr "Signatur mit Nebenschlüssel"

#: ui/gcr-gnupg-renderer.c:524
msgid "Primary key binding signature"
msgstr "Signatur mit Hauptschlüssel"

#: ui/gcr-gnupg-renderer.c:526
msgid "Signature directly on key"
msgstr "Signatur gehört direkt zum Schlüssel"

#: ui/gcr-gnupg-renderer.c:528
msgid "Key revocation signature"
msgstr "Signatur der Zertifikatrücknahmeliste"

#: ui/gcr-gnupg-renderer.c:530
msgid "Subkey revocation signature"
msgstr "Untergeordnete Signatur der Zertifikatrücknahmeliste"

#: ui/gcr-gnupg-renderer.c:532
msgid "Certification revocation signature"
msgstr "Signatur zum Widerruf des Zertifikats"

#: ui/gcr-gnupg-renderer.c:534
msgid "Timestamp signature"
msgstr "Zeitstempel-Signatur"

#: ui/gcr-gnupg-renderer.c:536
msgid "Third-party confirmation signature"
msgstr "Fremde Bestätigungssignatur"

#: ui/gcr-gnupg-renderer.c:589 ui/gcr-gnupg-renderer.c:597
msgid "Class"
msgstr "Klasse"

#: ui/gcr-gnupg-renderer.c:591
msgid "Local only"
msgstr "Nur lokal"

#: ui/gcr-gnupg-renderer.c:593
msgid "Exportable"
msgstr "Exportierbar"

#: ui/gcr-gnupg-renderer.c:611
msgid "Revocation Key"
msgstr "Widerrufungsschlüssel"

#: ui/gcr-gnupg-renderer.c:625 ui/gcr-gnupg-renderer.c:649
#: ui/gcr-gnupg-renderer.c:651
msgid "Fingerprint"
msgstr "Fingerabdruck"

#: ui/gcr-gnupg-renderer.c:740
msgid "Public Subkey"
msgstr "Öffentlicher Unterschlüssel"

#: ui/gcr-gnupg-renderer.c:742
msgid "Secret Key"
msgstr "Geheimer Schlüssel"

#: ui/gcr-gnupg-renderer.c:744
msgid "Secret Subkey"
msgstr "Versteckter Unterschlüssel"

#: ui/gcr-import-button.c:118
msgid "Initializing…"
msgstr "Initialisieren …"

#: ui/gcr-import-button.c:126
msgid "Import is in progress…"
msgstr "Import läuft …"

#: ui/gcr-import-button.c:133
#, c-format
msgid "Imported to: %s"
msgstr "Importiert nach: %s"

#: ui/gcr-import-button.c:153
#, c-format
msgid "Import to: %s"
msgstr "Importieren nach: %s"

#: ui/gcr-import-button.c:166
msgid "Cannot import because there are no compatible importers"
msgstr ""
"Importieren nicht möglich, da keine kompatiblen Importierer verfügbar sind"

#: ui/gcr-import-button.c:175
msgid "No data to import"
msgstr "Keine Dateien zu importieren"

#: ui/gcr-key-renderer.c:89
msgid "Key"
msgstr "Schlüssel"

#: ui/gcr-key-renderer.c:355
msgid "Private RSA Key"
msgstr "Privater RSA-Schlüssel"

#: ui/gcr-key-renderer.c:357
msgid "Private DSA Key"
msgstr "Privater DSA-Schlüssel"

#: ui/gcr-key-renderer.c:359
msgid "Private Elliptic Curve Key"
msgstr "Privater elliptischer Kurvenschlüssel"

#: ui/gcr-key-renderer.c:364 ui/gcr-key-renderer.c:366
msgid "Public DSA Key"
msgstr "Öffentlicher DSA-Schlüssel"

#: ui/gcr-key-renderer.c:368
msgid "Public Elliptic Curve Key"
msgstr "Öffentlicher elliptischer Kurvenschlüssel"

#: ui/gcr-key-renderer.c:377
#, c-format
msgid "%u bit"
msgid_plural "%u bits"
msgstr[0] "%u Bit"
msgstr[1] "%u Bit"

#: ui/gcr-key-renderer.c:378
msgid "Strength"
msgstr "Länge"

#. Fingerprints
#: ui/gcr-key-renderer.c:402
msgid "Fingerprints"
msgstr "Fingerabdrücke"

#: ui/gcr-key-renderer.c:406
msgid "SHA1"
msgstr "SHA1"

#: ui/gcr-key-renderer.c:411
msgid "SHA256"
msgstr "SHA256"

#. Add our various buttons
#: ui/gcr-pkcs11-import-dialog.c:104 ui/gcr-prompt-dialog.c:605
#: ui/gcr-certificate-exporter.c:229 ui/gcr-certificate-exporter.c:306
msgid "_Cancel"
msgstr "A_bbrechen"

#: ui/gcr-pkcs11-import-dialog.c:106 ui/gcr-prompt-dialog.c:608
msgid "_OK"
msgstr "_OK"

#: ui/gcr-pkcs11-import-dialog.c:179
msgid "Automatically chosen"
msgstr "Automatisch ausgewählt"

#: ui/gcr-pkcs11-import-dialog.c:263 ui/gcr-pkcs11-import-interaction.c:142
#: ui/gcr-pkcs11-import-interaction.c:161
#, c-format
msgid "The user cancelled the operation"
msgstr "Der Benutzer brach den Vorgang ab"

#: ui/gcr-pkcs11-import-dialog.ui:31
msgid "In order to import, please enter the password."
msgstr "Um den Import zu starten, geben Sie bitte das Passwort ein."

#. The password label
#: ui/gcr-pkcs11-import-dialog.ui:66 ui/gcr-prompt-dialog.c:666
msgid "Password:"
msgstr "Passwort:"

#: ui/gcr-pkcs11-import-dialog.ui:80
msgid "Token:"
msgstr "Token:"

#: ui/gcr-pkcs11-import-dialog.ui:178
msgid "Label:"
msgstr "Bezeichnung:"

#: ui/gcr-pkcs11-import-dialog.ui:233
msgid "Import settings"
msgstr "Einstellungen importieren"

#. The confirm label
#: ui/gcr-prompt-dialog.c:683
msgid "Confirm:"
msgstr "Bestätige:"

#: ui/gcr-prompt-dialog.c:751
msgid "Passwords do not match."
msgstr "Passwörter stimmen nicht überein."

#: ui/gcr-prompt-dialog.c:758
msgid "Password cannot be blank"
msgstr "Passwortfeld kann nicht leer sein"

#: ui/gcr-prompter.desktop.in.in:3
msgid "Access Prompt"
msgstr "Zugangsnachfrage"

#: ui/gcr-prompter.desktop.in.in:4
msgid "Unlock access to passwords and other secrets"
msgstr "Den Zugang zu Passwörtern und anderen Geheimnissen entsperren"

#: ui/gcr-certificate-exporter.c:226
msgid "A file already exists with this name."
msgstr "Eine Datei dieses Namens existiert bereits."

#: ui/gcr-certificate-exporter.c:227
msgid "Do you want to replace it with a new file?"
msgstr "Wollen Sie sie durch eine neue Datei ersetzen?"

#: ui/gcr-certificate-exporter.c:230
msgid "_Replace"
msgstr "_Ersetzen"

#: ui/gcr-certificate-exporter.c:260
#, c-format
msgid "The operation was cancelled."
msgstr "Der Vorgang wurde abgebrochen."

#: ui/gcr-certificate-exporter.c:304
msgid "Export certificate"
msgstr "Zertifikat exportieren"

#: ui/gcr-certificate-exporter.c:307
msgid "_Save"
msgstr "_Speichern"

#: ui/gcr-certificate-exporter.c:316
msgid "Certificate files"
msgstr "Zertifikatdateien"

#: ui/gcr-certificate-exporter.c:327
msgid "PEM files"
msgstr "PEM-Dateien"

#: ui/gcr-unlock-options-widget.ui:16
msgid "Automatically unlock this keyring whenever I’m logged in"
msgstr "Diesen Schlüsselbund beim Anmelden automatisch entsperren"

#: ui/gcr-unlock-options-widget.ui:31
msgid "Lock this keyring when I log out"
msgstr "Diesen Schlüsselbund beim Abmelden automatisch sperren"

#: ui/gcr-unlock-options-widget.ui:53
msgid "Lock this keyring after"
msgstr "Diesen Schlüsselbund sperren nach"

#: ui/gcr-unlock-options-widget.ui:67
msgid "Lock this keyring if idle for"
msgstr "Diesen Schlüsselbund bei Untätigkeit sperren nach"

#. Translators: The 'minutes' from 'Lock this keyring if idle for x minutes'.
#: ui/gcr-unlock-options-widget.ui:103
msgid "minutes"
msgstr "Minuten"

#: ui/gcr-unlock-renderer.c:68
#, c-format
msgid "Unlock: %s"
msgstr "Entsperren: %s"

#: ui/gcr-unlock-renderer.c:122
msgid "Password"
msgstr "Passwort"

#: ui/gcr-unlock-renderer.c:274
#, c-format
msgid ""
"The contents of “%s” are locked. In order to view the contents, enter the "
"correct password."
msgstr ""
"Der Inhalt von »%s« ist gesperrt. Um den Inhalt anzeigen zu können, geben "
"Sie das korrekte Passwort ein."

#: ui/gcr-unlock-renderer.c:277
msgid ""
"The contents are locked. In order to view the contents, enter the correct "
"password."
msgstr ""
"Der Inhalt ist gesperrt. Um den Inhalt anzeigen zu können, geben Sie das "
"korrekte Passwort ein."

#: ui/gcr-viewer.desktop.in.in:3
msgid "View file"
msgstr "Datei betrachten"

#: ui/gcr-viewer-tool.c:40
msgid "GCR Certificate and Key Viewer"
msgstr "GCR Zertifikat- und Schlüsselbetrachter"

#: ui/gcr-viewer-tool.c:47
msgid "Show the application's version"
msgstr "Die Version der Anwendung anzeigen"

#: ui/gcr-viewer-tool.c:49
msgid "[file...]"
msgstr "[Datei …]"

#: ui/gcr-viewer-tool.c:100
msgid "- View certificate and key files"
msgstr "- Zertifikat- und Schlüsseldateien anzeigen"

#: ui/gcr-viewer-tool.c:114 ui/gcr-viewer-widget.c:669
msgid "Certificate Viewer"
msgstr "Zertifikatbetrachter"

#: ui/gcr-viewer-widget.c:189
msgid "The password was incorrect"
msgstr "Das Passwort war nicht korrekt"

#: ui/gcr-viewer-window.c:74
msgid "Imported"
msgstr "Importiert"

#: ui/gcr-viewer-window.c:78
msgid "Import failed"
msgstr "Der Vorgang ist fehlgeschlagen"

#: ui/gcr-viewer-window.c:105
msgid "Import"
msgstr "Importieren"

#: ui/gcr-viewer-window.c:114
msgid "_Close"
msgstr "S_chließen"
